xhost +local:root
docker run -it --rm --net=host --volume="/dev:/dev" \
	 -e DISPLAY=unix$DISPLAY \
	 -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
	 --privileged \
    plankton_foxy:latest \
	 /bin/bash
