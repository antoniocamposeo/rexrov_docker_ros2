ARG ROS2_DIST=foxy
 
FROM osrf/ros:${ROS2_DIST}-desktop as base

# Install Python dependencies #ros-${ROS2_DIST}-ros-base
RUN apt-get update || true && \
  apt-get install --no-install-recommends -y \
  apt-get install ros-$ROS_DISTRO-plotjuggler-ros \
  ros-foxy-ament-cmake-clang-format \
  ros-foxy-image-transport \
  ros-foxy-image-transport-plugins \
  ros-foxy-diagnostic-updater \
  ros-foxy-xacro \
  ros-foxy-gazebo-ros \
  ros-foxy-gazebo-ros-pkgs \
  python3-flake8-docstrings \
  python3-pytest \
  python3-pip \
  ros-dev-tools \
  v4l-utils && \
  pip3 install \
  argcomplete \
  numpy \
  empy \
  lark && \
  rm -rf /var/lib/apt/lists/*

# Disable apt-get warnings
RUN apt-get update || true && apt-get install -y --no-install-recommends apt-utils dialog && \
  rm -rf /var/lib/apt/lists/*

RUN sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list' 
RUN  /bin/bash -c "wget https://packages.osrfoundation.org/gazebo.key -O - || apt-key add -"

# Create Colcon workspace with external dependencies
RUN mkdir -p /home/ros2_ws/src

# Setup Sources
RUN apt-get -y update || true && \
  apt-get install --no-install-recommends -y software-properties-common && \
  add-apt-repository universe && \
  apt-get install -y curl && \
  apt-get install -y nano && \  
  curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg && \
  echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" | tee /etc/apt/sources.list.d/ros2.list > /dev/null && \
  rm -rf /var/lib/apt/lists/*

# Download Plakton for ROS2 Foxy 
WORKDIR /home/ros2_ws/src
RUN git clone https://github.com/Liquid-ai/Plankton.git 


#Build the base Colcon workspace, installing dependencies first.
# WORKDIR /home/ros2_ws/
# RUN /bin/bash -c "source /opt/ros/foxy/setup.bash; colcon build --packages-up-to plankton "
RUN /bin/bash -c "source /opt/ros/foxy/setup.bash"

# Setup environment variables
COPY ros_entrypoint.sh /sbin/ros_entrypoint.sh
RUN sudo chmod 755 /sbin/ros_entrypoint.sh

ENTRYPOINT ["/sbin/ros_entrypoint.sh"]
WORKDIR /home 
