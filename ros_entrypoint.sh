#!/bin/bash
set -e

# setup ros2 environment
source "/opt/ros/foxy/setup.bash" --
#source "/root/ros2_ws/install/local_setup.bash" --

# Welcome information
echo "Plankton ROS2 Docker Image"
echo "---------------------"
echo 'ROS distro: ' $ROS2_DIST
echo 'DDS middleware: ' $RMW_IMPLEMENTATION 
echo "---"  
echo 'Available packages:'
ros2 pkg list
echo "---------------------"    
exec "$@"
