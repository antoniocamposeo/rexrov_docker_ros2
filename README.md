# Installation 
To install docker container, follow these steps: 

` https://gitlab.com/antoniocamposeo/rexrov_docker_ros2 ` 

Go in folder from terminal and run the commmand: `sudo ./build.sh`

Once the building process has terminated check docker images. Run `sudo docker images` into another terminal, and check if the image has been created. 

To start the docker container, go in the folder uuvsim_muvic_docker in the terminal. Run `sudo ./run.sh`

Now you are in the container. 

# Test Muvic with Plakton Simulation 

Once the container is started, is needed to build the ros2 pakages with colcon build.
Use this commands:
  `source /opt/ros/foxy/setup.bash`
  `cd ~/ros2_ws` 
  `colcon build --packages-up-to plankton`
    

 Note: Every time you open a new terminal with the container, you need to source 3 different files described below to configure ROS 2 and Gazebo environment variables. Write the following each time you start a new terminal to deal with ROS 2 / Gazebo stuff, or prefer to add them at the end of your .bashrc file with `gedit ~/.bashrc`. For the latter, don’t forget to source your .bashrc to enforce the update after saving these changes, or open a fresh terminal.  

   - For ROS 2 variables  
`source /opt/ros/foxy/setup.bash`  
   - For your installation workspace (change the path accordingly)  
`source /home/ros2_ws/install/setup.bash`  
   - For Gazebo   
`source /usr/share/gazebo/setup.sh`


Let's start testing RexRov . 
Open a new terminal (don’t forget to source ROS 2 / Gazebo files if necessary) and write as below to open a gazebo world:  
`ros2 launch uuv_gazebo_worlds ocean_waves.launch`

Open again a new terminal (don't forget to source ROS 2 / Gazebo files if necessary), and spawn the rexrov robot:  
`ros2 launch uuv_descriptions upload_rexrov.launch mode:=default x:=0 y:=0 z:=-20 namespace:=rexrov`

Add a joystick node to control it remotely (new terminal needed) :  
`ros2 launch uuv_control_cascaded_pid joy_velocity.launch uuv_name:=rexrov model_name:=rexrov joy_id:=0`


# How to use keyboard teleoperation 
To control the rouv with the keyboard, is necessary modify the launch file of uuv_control_cascaded_pid. 
Go to the directory uuv_control/uuv_control_cascaded_pids/launch, open key_board_velocity.launch and comment this part:
``` 
<include file="$(find-pkg-share uuv_teleop)/launch/uuv_keyboard_teleop.launch">
    <arg name="uuv_name" value="$(var uuv_name)"/>
    <arg name="output_topic" value="cmd_vel"/>
    <arg name="message_type" value="twist"/>
</include>
  
  ```
After that, rebuild the package with : `colcon build --packages-up-to plankton` 

And start testing the RexROV:

Open a new terminal (don’t forget to source ROS 2 / Gazebo files if necessary) and write as below to open a gazebo world:  
`ros2 launch uuv_gazebo_worlds ocean_waves.launch`

Open again a new terminal (don't forget to source ROS 2 / Gazebo files if necessary), and spawn the rexrov robot:  
`ros2 launch uuv_descriptions upload_rexrov.launch mode:=default x:=0 y:=0 z:=-20 namespace:=rexrov`

Add a control node to manage the input from the keyboard:  
`ros2 launch uuv_control_cascaded_pid key_board_velocity.launch uuv_name:=rexrov model_name:=rexrov`

Open again a new terminal (don't forget to source ROS 2 / Gazebo files if necessary):  
`ros2 run uuv_teleop  vehicle_keyboard_teleop.py --ros-args -r  output:=rexrov/cmd_vel` 

TODO :
- Generalize the control with keyboard, 
- Undestand why the launch file of keyboard control not work. 



## Utils 
Ros2 command: 

```
ros2 run <package_name> <executable_name> 
ros2 launch <package_name> <launch_file_name>

ros2 node list
ros2 node info <node_name>
ros2 topic list 
ros2 topic echo <topic_name>
``` 

Docker command:
```
sudo docker images                          # list of images
sudo docker ps                              # list of running container
sudo docker ps -a                           # list of container stopped
sudo docker rmi -f tag_name or id images    # remove an image
sudo docker stop $(docker ps -aq)           # stop all containers
sudo docker rm $(docker ps -aq)             # remove all containers
sudo docker network namenet                 # create a bridge network
sudo docker run -d -p -network namenet      # run network
sudo docker build path                      # build a Dockerfile
sudo docker run -it <tag_name_image>
sudo docker exec -it <container> bash
sudo docker commit <id container or name > <new image or old images to be saved>
```
